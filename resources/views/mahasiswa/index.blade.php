@extends('layout/main')
@section('title', 'Daftar Mahasiswa')
@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Mahasiswa</h1>

            <table class="table">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Program Studi</th>
                        <th scope="col">Angkatan</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        @foreach ($mahasiswa as $mhs)
                        <th scope="col">{{$mhs}}</th>
                        @endforeach
                        <td>
                            <a href="" span class="badge bg-success">EDIT</a>
                            <a href="" span class="badge bg-danger">DELETE</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection